Neutrino greatly lessens the dependencies between logic and layout by providing the core JS functionality that developers can then access through declarative markup.


This gives creative developers the ability to quickly prototype, produce, and deploy engaging interactive experiences without touching javascript, and  perform on all your devices from one code base!
